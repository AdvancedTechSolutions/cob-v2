// Action Items
-


// Process to install and run
npm i
npm start

// proto flow:
1) screen number = inventory management (first screen)
click on the list get detail
hit "work item:" get the node graph (screen 3)

click on policies -> screen 4

tap on edit of aetna card #2 (card flip/transition happens)

save changes => reverse animations and go back screen 4

click health order of liability => screen 6

back to work queue goes to screen screen 2

save and close goes to screen 1


// Process for deploying to Surge
- in .env => switch from "development" to "production"
- copy over the deploy.sh file from the boilerplate
- make sure your deploy value in package.json is the same as in the boiler plate
- npm run build
- cd into the new dist folder
- create a .surgeignore file with this as it's contents:
!node_modules/
!node_modules
- create a file 200.html and copy the contents of index .html
- put / in front of the 2 app. and 1 vendor. at the end of the file
- then run surge as normal from the dist folder

