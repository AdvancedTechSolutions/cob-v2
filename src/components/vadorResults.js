import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'
import '../styles/style.scss';
import '../styles/horol.css';

class VadorResults extends Component {

  render(){
    return (
      <div className="horol_container">
        <div className="horol_list">
          <div className="Head">
            <div className="sort">Sort&nbsp;<i className="fa fa-caret-down" aria-hidden="true"></i></div>
            <h1>COBD CMS Search Results</h1>
          </div>
          <div className="items">
            <div className="record_current">
              <div className="info">12-12-2015<br/>
              HICN Number<br/>
              123455555</div>
            </div>
            <div className="record">
              <div className="info">12-12-2015<br/>
              HICN Number<br/>
            123455555</div>
            </div>
            <div className="record">
              <div className="info">12-12-2015<br/>
              HICN Number<br/>
            123455555</div>
            </div>
            <div className="record">
              <div className="info">12-12-2015<br/>
              HICN Number<br/>
            123455555</div>
            </div>
            <div className="record">
              <div className="info">12-12-2015<br/>
              HICN Number<br/>
            123455555</div>
            </div>
            <div className="record">
              <div className="info">12-12-2015<br/>
              HICN Number<br/>
            123455555</div>
            </div>
            <div className="record">
              <div className="info">12-12-2015<br/>
              HICN Number<br/>
            123455555</div>
            </div>
          </div>
        </div>
        <div className="horol_details">
          <div className="actions">
            <div className="horol_arrow"><Link to="/vador"><i className="fa fa-long-arrow-left" aria-hidden="true"></i>&nbsp;Back to Search</Link></div>
            <div className="horol_close"><Link to="/work">X</Link></div>
          </div>

          <table>
            <tbody>
              <tr>
                <td className="left">
                  <h4>File Date (MM-DD-YYYY</h4>
                  <h3>12-12-2015</h3>
                </td>
                <td className="right">
                  <h4>CMS Contract Number</h4>
                  <h3>1230000000000</h3>
                </td>
              </tr>
              <tr>
                <td className="left">
                  <h4>HICN  Number</h4>
                <h3>12345555</h3>
                </td>
                <td className="right">
                  <h4>Supplemental Type Code</h4>
                <h3>54321000000</h3>
                </td>
              </tr>
              <tr>
                <td className="left">
                  <h4>SSN</h4>
                <h3>000-00-0000</h3>
                </td>
                <td className="right">
                  <h4>ICAR Code</h4>
                  <h3>&nbsp;</h3>
                </td>
              </tr>
              <tr>
                <td className="left_last">
                  <h4>Florida Blue Contract Number</h4>
                  <h3>H06789000</h3>
                </td>
                <td className="right_last">
                  <h4>Error Codes</h4>
                  <h3>5</h3>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}



export default VadorResults
