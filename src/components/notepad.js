import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import Breadcrumbs from './BreadCrumbs';

const styles = {
  notepad_pad: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: '0px',
    right: '0px',
    margin: '0px 20px 25px 0px'
  },
};

class Notepad extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    return (
      <div className='notepad_pad' style={styles.notepad_pad}>
        <img src={this.props.notepadImg} />
      </div>
    );
  }
}

export default Notepad;
