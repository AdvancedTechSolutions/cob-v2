import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import Dragula from 'react-dragula';
import '../styles/style.scss';
import '../styles/dool.css';

class Dool extends Component {

  constructor (props) {
    super(props);
    this.recalculate = this.recalculate.bind(this);
  }

  render(){
    return (

      <div id="dool_container" className="ool_container">

      <div id="ool_header" className="ool_row">
      <table>
      <td className="sort">&nbsp;</td>
    <td className="orderNum">Order</td>
      <td className="company">Company Name</td>
    <td>Policy #</td>
    <td className="ohi">OHI</td>
      <td className="status">OOL Status</td>
    <td className="date">DOROL Eff. Date</td>
    <td className="date">DOROL Term Date</td>
    </table>
    </div>

    <div className='containerDrag' ref={this.dragulaDecorator}>
        <div id="" className="ool_row">
          <table>
          <td className="sort"><i className="fa fa-bars" aria-hidden="true"></i></td>
          <td className="orderNum"><div id="" className="numActiveDool">1</div></td>
          <td className="company">Florida Blue</td>
        <td>227841535</td>
        <td className="ohi">Y</td>
          <td className="status">Y - Set</td>
          <td className="date"><div id="" className="dateBox">01/01/2016</div></td>
          <td className="date"></td>
          </table>
          </div>
          <div id="" className="ool_row">
          <table>
          <td className="sort"><i className="fa fa-bars" aria-hidden="true"></i></td>
          <td className="orderNum"><div id="" className="numActiveDool">2</div></td>
          <td className="company">Aetna</td>
        <td>ABC123</td>
        <td className="ohi">Y</td>
          <td className="status">Y - Set</td>
          <td className="date"><div id="" className="dateBox">03/31/2016</div></td>
          <td className="date"></td>
          </table>
        </div>
      </div>
      <div id="" className="ool_row">
      <table>
      <td className="sort"></td>
      <td className="orderNum"><div id="" className="numInactive">1</div></td>
      <td className="company">Florida Blue</td>
    <td>227841535</td>
    <td className="ohi">Y</td>
      <td className="status">Y - Set</td>
      <td className="date"><div id="" className="dateBox">01/01/2016</div></td>
      <td className="date"><div id="" className="dateBox">01/01/2016</div></td>
      </table>
      </div>
      <div id="" className="ool_row">
      <table>
      <td className="sort"></td>
      <td className="orderNum"><div id="" className="numInactive">2</div></td>
      <td className="company">Florida Blue</td>
    <td>227841535</td>
    <td className="ohi">Y</td>
      <td className="status">Y - Set</td>
      <td className="date"><div id="" className="dateBox">01/01/2016</div></td>
      <td className="date"><div id="" className="dateBox">01/01/2016</div></td>
      </table>
      </div>





      </div>
    )
  }

  dragulaDecorator = (componentBackingInstance) => {
    if (componentBackingInstance) {
      let options = { };
      var drake = Dragula([componentBackingInstance], options);
      console.log(drake.containers);
      drake.on('dragend', (el) => {
        this.recalculate();
    });
    }
  }
  recalculate() {
    var sortRowNums = document.getElementsByClassName("numActiveDool");
    var l = sortRowNums.length;
    var i;
    var cur;

    for(i=0; i<l; i++) {
      cur = sortRowNums[i];
      // do stuff with cur here
      cur.innerHTML = i + 1;
    }
  }
}

export default Dool
