import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import Breadcrumbs from './BreadCrumbs';

const styles = {
  note_widget: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: '0px',
    left: '0px',
    margin: '0px 0px 25px 20px'
  },
};

class NodeWidget extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    return (
      <div className='note_widget' style={styles.note_widget}>
  <img src={this.props.nodeWidgetImg} />
  </div>
  );
  }
}

export default NodeWidget;
