import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'
import '../styles/style.scss';
import '../styles/horol.css';

class Horol extends Component {

  render(){
    return (
      <div className="horol_container">
        <div className="horol_list">
          <div className="Head">
            <div className="sort">Sort&nbsp;<i className="fa fa-caret-down" aria-hidden="true"></i></div>
            <h1>Health Order of Liability</h1>
          </div>
          <div className="items">
            <div className="record_current">
              <div className="number"><div className="circle">1</div></div>
              <div className="info">BCBSFL BRP 0000 PH3<br/>
    Policy Number<br/>
    123455555</div>
              <div className="more">Type<br/>
      G<br/>
      EFCV<br/>
      MM/DD/YYY</div>
            </div>
            <div className="record">
              <div className="number"><div className="circle">2</div></div>
              <div className="info">Adjustco<br/>
    Policy Number<br/>
    123455555</div>
              <div className="more">
                Type<br/>
                G<br/>
                EFCV<br/>
                MM/DD/YYYY
              </div>
            </div>
            <div className="record">
              <div className="number"><div className="circle">3</div></div>
              <div className="info">Other Insurance<br/>
    Policy Number<br/>
    123455555</div>
              <div className="more">
                Type<br/>
                G<br/>
                EFCV<br/>
                MM/DD/YYYY
              </div>
            </div>
            <div className="record">
              <div className="number"><div className="circle">4</div></div>
              <div className="info">Other Insurance<br/>
                Policy Number<br/>
                123455555</div>
              <div className="more">
                Type<br/>
                G<br/>
                EFCV<br/>
                MM/DD/YYYY
                </div>
              </div>
              <div className="record">
                <div className="number"><div className="circle">5</div></div>
                <div className="info">Other Insurance<br/>
                  Policy Number<br/>
                  123455555</div>
                <div className="more">
                  Type<br/>
                  G<br/>
                  EFCV<br/>
                  MM/DD/YYYY
                </div>
              </div>
              <div className="record">
                <div className="number"><div className="circle">6</div></div>
                <div className="info">Other Insurance<br/>
                  Policy Number<br/>
                  123455555
                </div>
                <div className="more">
                    Type<br/>
                    G<br/>
                    EFCV<br/>
                    MM/DD/YYY&
                  </div>
                </div>
          </div>
        </div>
        <div className="horol_details">
          <div className="actions">
            <div className="horol_arrow"><i className="fa fa-long-arrow-left" aria-hidden="true"></i></div>
            <div className="horol_close"><Link to="/member">X</Link></div>
          </div>
          <div className="horol_title">
            <h1>Health Order of Liablity&nbsp;<Link to="/horoledit"><i className="fa fa-pencil-square-o" aria-hidden="true"></i></Link></h1>
            <h2>BCBSFL BRP 0000 PH3</h2>
          </div>
          <table>
            <tbody>
              <tr>
                <td className="left">
                  <h4>Mbr Last Name</h4>
                  <h3>McGary</h3>
                </td>
                <td className="middle">
                  <h4>Mbr First Name</h4>
                  <h3>Katie</h3>
                </td>
                <td className="right">
                  <h4>Mbr Mid Int</h4>
                  <h3>&nbsp;</h3>
                </td>
              </tr>
              <tr>
                <td className="left">
                  <h4>Policy  Number</h4>
                <h3>12345555</h3>
                </td>
                <td className="middle">
                  <h4>EFCV</h4>
                <h3>04292004</h3>
                </td>
                <td className="right">
                  <h4>Mbr</h4>
                <h3>1</h3>
                </td>
              </tr>
              <tr>
                <td className="left">
                  <h4>Contract Number</h4>
                <h3>123455555</h3>
                </td>
                <td className="middle">
                  <h4>Health Care ID</h4>
                <h3>H23455555</h3>
                </td>
                <td className="right">
                  &nbsp;
                </td>
              </tr>
              <tr>
                <td className="left_last">
                  <h4>Research Date</h4>
                  <h3>(MM/DD/YYY)</h3>
                </td>
                <td className="middle_last">
                  <h4>Mbr PHI Address</h4>
                  <h3>12345 Main Street<br/>Jacksonville, FL 32556</h3>
                </td>
                <td className="right_last">&nbsp;</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}



export default Horol
