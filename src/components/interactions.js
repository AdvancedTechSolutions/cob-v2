import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'
import '../styles/style.scss';

const styles = {
  btn_login: {
    backgroundColor: '#58b651',
    color: '#fff',
    position: 'relative',
    padding: '3px 10px 3px 10px',
    borderRadius: 'none'
  },
  div_login: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '300px',
    marginTop: '150px'
  }
};

class Interactions extends Component {

  render(){
    return (
      <div class="workQList">
        <div id="" className="search">Search</div>
        <div id="" className="filter">Filter</div>
        <div id="" className="items">
          <div id="" className="item current">List Item - current</div>
          <div id="" className="item">List Item</div>
          <div id="" className="item">List Item</div>
          <div id="" className="item">List Item</div>
          <div id="" className="item">List Item</div>

        </div>
      </div>
    )
  }
}

export default Interactions
