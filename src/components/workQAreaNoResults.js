import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import '../styles/style.scss';

class workQAreaNoResults extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render(){
    return (
      <div className="no_results" >
        No Results Found
      </div>
    )
  }
}

export default (workQAreaNoResults);


