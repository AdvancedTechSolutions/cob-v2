import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import Breadcrumbs from './BreadCrumbs';

const styles = {
  topBar: {
    backgroundColor: 'transparent',
    position: 'relative',
    padding: '20px 25px 0px'
  }
};

class Footer extends Component {
  constructor(props) {
    super(props);

    this.hideQLink = this.hideQLink.bind(this);
    this.state = {};
  }

  toggles(obj){
    this.state = {};
    this.setState(obj);
  }


  render(){
    return (
      <div id="footer" className="">
        <div id="" className="back">
        <div className="pad"><Link onClick={this.hideQLink} id="back_to_q_link" to="/"><img src={this.props.footerImage1} width="18" height="20" alt=""/>Back to Work Queue</Link></div>
      </div>
    <div id="" className="logo">&nbsp;</div>
      </div>
    );
  }
  hideQLink (event) {
  console.log('hide q link');
    document.getElementById('footer').style.visibility = 'hidden';
    // also hide any open windows
    document.getElementById('policies_div').style.display = 'none';
    document.getElementById('hool_container').style.display = 'none';
    document.getElementById('policy_details_div').style.display = 'none';
    document.getElementById('dool_container').style.display = 'none';
    document.getElementById('claims_div').style.display = 'none';
    document.getElementById('claims_details_div').style.display = 'none';
    document.getElementById('correspond_container').style.display = 'none';
    document.getElementById('accidents_container').style.display = 'none';
}
}





export default connect(state=>state,routerActions)(Footer);
