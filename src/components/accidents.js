import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import Dragula from 'react-dragula';
import '../styles/style.scss';
import '../styles/accidents.css';

class Accidents extends Component {

  constructor (props) {
    super(props);
    this.recalculate = this.recalculate.bind(this);
  }

  render(){
    return (

      <div id="accidents_container" className="ool_container">
      <div id="ool_header" className="ool_row">
      <table>
        <tr>
      <td className="sort">Type</td>
    <td className="date">Date</td>
      <td className="company">Summary</td>
    <td>File(s)</td>
      </tr>
    </table>
    </div>
      <div id="" className="ool_row">
      <table><tr>
      <td className="sort">Letter</td>
      <td className="date"><div id="" className="dateBox">03/01/2016</div></td>
      <td className="company">unable to process the claim</td>
    <td><a target="_blank" href="docs/sample_letter.pdf">letter.pdf</a></td>
      </tr><tr>
      <td className="sort">Phone Call</td>
      <td className="date"><div id="" className="dateBox">02/08/2016</div></td>
      <td className="company">Left Message</td>
    <td>N/A</td>
    </tr><tr>
    <td className="sort">Letter</td>
      <td className="date"><div id="" className="dateBox">12/15/2015</div></td>
      <td className="company">unable to process the claim</td>
    <td><a target="_blank" href="docs/sample_letter.pdf">letter.pdf</a></td>
      </tr>
      </table>
      </div>
      </div>
    )
  }



  dragulaDecorator = (componentBackingInstance) => {
    if (componentBackingInstance) {
      let options = { };
      var drake = Dragula([componentBackingInstance], options);
      console.log(drake.containers);
      drake.on('dragend', (el) => {
        this.recalculate();
    });
    }
  }
  recalculate() {
    var sortRowNums = document.getElementsByClassName("numActive");
    var l = sortRowNums.length;
    var i;
    var cur;

    for(i=0; i<l; i++) {
      cur = sortRowNums[i];
      // do stuff with cur here
      cur.innerHTML = i + 1;
    }
  }
}

export default Accidents
