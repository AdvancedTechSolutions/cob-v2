import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { push } from 'react-router-redux';
import { routerActions } from 'react-router-redux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import StatGauges from './statGauges';
import '../styles/menu_list.css';

const styles = {
  sublistShow: {
    display: 'block'
  },
  vador_close: {
    textAlign: 'right',
  }
};

class MenuList extends Component {
  static propTypes = {
    routerActions: PropTypes.object,
  };
  constructor (props) {
    super(props);

    this.clickHandler = this.clickHandler.bind(this);
    this.showQLink = this.showQLink.bind(this);
    this.cancelQLink = this.cancelQLink.bind(this);
  }

  render(){
    return (
      <div id="menu_list_container">
        <div id="menu">
          <nav>
            <ul>
              <li><div className="subNav"><i className="fa fa-home" aria-hidden="true"></i>My Dashboard</div></li>
              <li><div id="menuWorkQueue" className="subNav"><i className="fa fa-folder-open" aria-hidden="true"></i>My Work Queue</div>
                <ul className="sublistShow" >
                <li><div id="" className=""><i className="fa fa-file" aria-hidden="true"></i>&nbsp;Claims</div></li>
                <li className="active"><div id="" className=""><i className="fa fa-sort-amount-asc" aria-hidden="true"></i>&nbsp;Order of Liability</div></li>
                <li><div id="" className=""><i className="fa fa-check-square-o" aria-hidden="true"></i>&nbsp;Compliance</div></li>
                </ul>
              </li>
              <li>
                <div className="subNav"><i className="fa fa-folder-open-o" aria-hidden="true"></i>All Work Queues</div>
                <ul className="out">
                <li><div id="" className="">Claims</div></li>
                <li><div id="" className="">Order of Liability</div></li>
                <li><div id="" className="">Compliance</div></li>
                </ul>
              </li>
              <li><div className="subNav"><i className="fa fa-exchange" aria-hidden="true"></i>Work Flow</div></li>
              <hr/>
              <li><div className="subNav"><i className="fa fa-file-o" aria-hidden="true"></i>Compliance Search</div></li>
              <li><div className="subNav"><Link to="/MemberSearch"><i className="fa fa-user" aria-hidden="true"></i>Member Search</Link></div></li>
            </ul>
          </nav>
        </div>
    <div id="list">
      <div id="" className="search">
      <table className="search_table">
      <tr>
      <td>Domain Team</td>
    <td>Filter By</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td><select><option>My Team</option></select></td>
    <td><select><option>Claim Exceptions</option></select></td>
    <td>
    <form className="form-inline" role="form">
      <div className="form-group has-feedback">
      <label className="control-label" for="inputSuccess4"></label>
      <input type="text" className="form-control" id="inputSuccess4"/>
      <span className="fa fa-search form-control-feedback" aria-hidden="true" ></span>
      </div>
      </form>
      </td>
      <td className="list_view"><div className="view_actions"><i className="fa fa-list" aria-hidden="true"></i>List&nbsp;&nbsp;<i className="fa fa-th" aria-hidden="true"></i>Tile</div></td>
      </tr>
      </table>




      </div>
      <div id="list_table" className="table">
      <table className="table table-hover">
      <thead>
      <th>Case #<i className="fa fa-sort" aria-hidden="true"></i></th>
      <th>Case Type<i className="fa fa-sort" aria-hidden="true"></i></th>
      <th>Status<i className="fa fa-sort" aria-hidden="true"></i></th>
      <th>Member<i className="fa fa-sort" aria-hidden="true"></i></th>
      <th>HICN<i className="fa fa-sort" aria-hidden="true"></i></th>
      <th>Received<i className="fa fa-sort" aria-hidden="true"></i></th>
      <th>Age<i className="fa fa-sort" aria-hidden="true"></i></th>
      <th>SEC<i className="fa fa-sort" aria-hidden="true"></i></th>
      <th>Follow Up<i className="fa fa-sort" aria-hidden="true"></i></th>
      <th>Opened By<i className="fa fa-sort" aria-hidden="true"></i></th>
      <th>Tracking Type<i className="fa fa-sort" aria-hidden="true"></i></th>
      </thead>
        <tbody>
        <tr onClick={this.clickHandler}>
          <td>144838025</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>47</td>
          <td>17</td>
          <td>04/14/2016 Up</td>
          <td>Reynolds, Ryan</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>887657654</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>33</td>
          <td>12</td>
          <td>04/14/2016 Up</td>
          <td>Richardson, Tyler</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>234143987</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>25</td>
          <td>6</td>
          <td>04/14/2016 Up</td>
          <td>White, Jack</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>08978921</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>65</td>
          <td>2</td>
          <td>04/14/2016 Up</td>
          <td>Black, Jack</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>323242521</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>33</td>
          <td>12</td>
          <td>04/14/2016 Up</td>
          <td>Richardson, Tyler</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>345612231</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>25</td>
          <td>6</td>
          <td>04/14/2016 Up</td>
          <td>White, Jack</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>308091832</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>65</td>
          <td>2</td>
          <td>04/14/2016 Up</td>
          <td>Black, Jack</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>43218788</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>33</td>
          <td>12</td>
          <td>04/14/2016 Up</td>
          <td>Hunter, Tyler</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>144838025</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>25</td>
          <td>6</td>
          <td>04/14/2016 Up</td>
          <td>White, Jack</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>55654321</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>65</td>
          <td>2</td>
          <td>04/14/2016 Up</td>
          <td>Black, Jack</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>4046632224</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>33</td>
          <td>12</td>
          <td>04/14/2016 Up</td>
          <td>Richardson, Tyler</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>144838025</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>25</td>
          <td>6</td>
          <td>04/14/2016 Up</td>
          <td>Rasul, Amir</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>4047237043</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>65</td>
          <td>2</td>
          <td>04/14/2016 Up</td>
          <td>Black, Jack</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>217289555</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>33</td>
          <td>12</td>
          <td>04/14/2016 Up</td>
          <td>Richardson, Tyler</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>144838025</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>25</td>
          <td>6</td>
          <td>04/14/2016 Up</td>
          <td>White, Jack</td>
          <td>Tracking Type</td>
        </tr>
        <tr onClick={this.clickHandler}>
          <td>144838025</td>
          <td>TK</td>
          <td>NE</td>
          <td>123456789</td>
          <td>254698792</td>
          <td>02/14/2016</td>
          <td>65</td>
          <td>2</td>
          <td>04/14/2016 Up</td>
          <td>Black, Jack</td>
          <td>Tracking Type</td>
        </tr>

      </tbody>
    </table>
      </div>
        <StatGauges/>
        </div>
        <div id="detail_slide">
          <Link onClick={this.showQLink} to="/member"><img src="images/im_detail_ph.png"/></Link><br/>
          <Link onClick={this.cancelQLink} to="/"><img src="images/im_detail_btn_ph.png"/></Link>
        </div>
      </div>
    )
  }

  clickHandler (event) {
    //this.props.routerActions.push('/member');
    document.getElementById('detail_slide').style.display = 'block';
  }
  showQLink (event) {
    //this.props.routerActions.push('/member');
    console.log('show q link');
    document.getElementById('footer').style.visibility = 'visible';
  }
  cancelQLink (event) {
    //this.props.routerActions.push('/member');
    console.log('cancel q link');
    document.getElementById('detail_slide').style.display = 'none';
  }
}

export default MenuList
