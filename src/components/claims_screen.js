import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import '../styles/claims.css';

const styles = {
  topBar: {
    backgroundColor: 'transparent',
    position: 'relative',
    padding: '20px 25px 0px'
  }
};

class Claims extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
    this.clickHandler = this.clickHandler.bind(this);
    this.showDetails = this.showDetails.bind(this);
  }

  toggles(obj){
    this.state = {};
    this.setState(obj);
  }


  render(){
    return (
      <div id="claims_div" className="expanded"><div className="stack">
      <div className="policy_card">
      <div className="card_header active" onClick={this.showDetails}>
      Q10000000496431129
    </div>
    <div className="card_body">
      <p>MEMBER NAME<br/>
    <strong>Katie McGary</strong></p>
      <p>DOS:<br/>
    10/13/2015</p>
      <p>ITS NO.<br/>
    --</p>
      <p> TOTAL CHARGED<br/>
    <strong>$124.00</strong></p>
      <p>MEMBER RESPONSIBILITY<br/>
    <strong>$25.00</strong></p>
      <p><strong>PROFESSIONAL</strong></p>

      </div>
      </div>
      <div className="policy_card">
      <div className="card_header active" onClick={this.showDetails}>
      Q10000000496431129
      </div>
      <div className="card_body">
      <p>MEMBER NAME<br/>
    <strong>Katie McGary</strong></p>
    <p>DOS:<br/>
    10/13/2015</p>
    <p>ITS NO.<br/>
    --</p>
    <p> TOTAL CHARGED<br/>
    <strong>$124.00</strong></p>
    <p>MEMBER RESPONSIBILITY<br/>
    <strong>$25.00</strong></p>
    <p><strong>PROFESSIONAL</strong></p>


    </div>
      </div>
      <div className="policy_card">
      <div className="card_header active" onClick={this.showDetails}>
      Q10000000496431129
    </div>
    <div className="card_body">
      <p>MEMBER NAME<br/>
    <strong>Katie McGary</strong></p>
    <p>DOS:<br/>
    10/13/2015</p>
    <p>ITS NO.<br/>
    --</p>
    <p> TOTAL CHARGED<br/>
    <strong>$124.00</strong></p>
    <p>MEMBER RESPONSIBILITY<br/>
    <strong>$25.00</strong></p>
    <p><strong>PROFESSIONAL</strong></p>

    </div>
    </div>
    <div className="policy_card">
      <div className="card_header active" onClick={this.showDetails}>
      Q10000000496431129
      </div>
      <div className="card_body">
      <p>MEMBER NAME<br/>
    <strong>Katie McGary</strong></p>
    <p>DOS:<br/>
    10/13/2015</p>
    <p>ITS NO.<br/>
    --</p>
    <p> TOTAL CHARGED<br/>
    <strong>$124.00</strong></p>
    <p>MEMBER RESPONSIBILITY<br/>
    <strong>$25.00</strong></p>
    <p><strong>PROFESSIONAL</strong></p>
    </div>
    </div>  <div className="policy_card">
      <div className="card_header active" onClick={this.showDetails}>
      Q10000000496431129
      </div>
      <div className="card_body">
      <p>MEMBER NAME<br/>
    <strong>Katie McGary</strong></p>
    <p>DOS:<br/>
    10/13/2015</p>
    <p>ITS NO.<br/>
    --</p>
    <p> TOTAL CHARGED<br/>
    <strong>$124.00</strong></p>
    <p>MEMBER RESPONSIBILITY<br/>
    <strong>$25.00</strong></p>
    <p><strong>PROFESSIONAL</strong></p>
    </div>
    </div>

    <div className="policy_card">
      <div className="card_header active" onClick={this.showDetails}>
      Q10000000496431129
      </div>
      <div className="card_body">
      <p>MEMBER NAME<br/>
    <strong>Katie McGary</strong></p>
    <p>DOS:<br/>
    10/13/2015</p>
    <p>ITS NO.<br/>
    --</p>
    <p> TOTAL CHARGED<br/>
    <strong>$124.00</strong></p>
    <p>MEMBER RESPONSIBILITY<br/>
    <strong>$25.00</strong></p>
    <p><strong>PROFESSIONAL</strong></p>
    </div>
    </div>

    </div>
      </div>

    );
  }

  showDetails(){
    document.getElementById('claims_details_div').style.display='block';
    document.getElementById('claims_div').style.display='none';
  }

  clickHandler (event) {
    //this.props.routerActions.push('/member');
    console.log('policy screen');
    // minimize the policy div
    document.getElementById('policies_div').style.transform='scale(.3)';
    document.getElementById('policies_div').style.top='200px';
    document.getElementById('policies_div').style.right='250px';
    document.getElementById('policies_div').style.opacity='0';
    setTimeout(this.showDetails, 800)
    // show and maximize the details div

    // hide the policy div and reset its css
    //document.getElementById('policies_div').style.display = 'none';
    //document.getElementById('policies_div').style.transform='scale(1)';
    //document.getElementById('policies_div').style.top='100px';
    //document.getElementById('policies_div').style.right='0px';
    //document.getElementById('policies_div').style.opacity='1';
  }
}

export default connect(state=>state,routerActions)(Claims);



