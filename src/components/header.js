import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import Breadcrumbs from './BreadCrumbs';

const styles = {
  topBar: {
    backgroundColor: 'transparent',
    position: 'relative',
    padding: '20px 25px 0px'
  }
};

class Header extends Component {
  constructor(props) {
    super(props);


    this.hideScreens = this.hideScreens.bind(this);

    this.state = {};
  }

  toggles(obj){
    this.state = {};
    this.setState(obj);
  }


  render(){
    return (
      <div id="header">
        <div className="icons">
          <div className="logo"><div className="pad"><img src={this.props.logoImage} width="73" height="30" alt=""/></div></div>
          <div className="actions"><Link to='/memberSearch' onClick={this.hideScreens}><i className="fa fa-search" aria-hidden="true"></i></Link><Link to='/'><img src={this.props.headerImage1} width="23" height="25" alt=""/></Link><img src={this.props.headerImage2} width="23" height="25" alt=""/><img src={this.props.headerImage3} width="30" height="30" alt=""/></div>
        </div>
        <div className="name">Katie McGary</div>
      </div>
    );
  }

  hideScreens (event) {
    console.log('hide screens');
    document.getElementById('footer').style.visibility = 'hidden';
    // also hide any open windows
    document.getElementById('policies_div').style.display = 'none';
    document.getElementById('hool_container').style.display = 'none';
    document.getElementById('policy_details_div').style.display = 'none';
    document.getElementById('dool_container').style.display = 'none';
    document.getElementById('claims_div').style.display = 'none';
    document.getElementById('claims_details_div').style.display = 'none';
    document.getElementById('correspond_container').style.display = 'none';
    document.getElementById('accidents_container').style.display = 'none';
  }
}

export default connect(state=>state,routerActions)(Header);
