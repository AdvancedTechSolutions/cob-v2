import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import '../styles/policies.css';

const styles = {
  topBar: {
    backgroundColor: 'transparent',
    position: 'relative',
    padding: '20px 25px 0px'
  }
};

class Policies extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
    this.clickHandler = this.clickHandler.bind(this);
    this.showDetails = this.showDetails.bind(this);
  }

  toggles(obj){
    this.state = {};
    this.setState(obj);
  }


  render(){
    return (
      <div id="policies_div" className="expanded"><div className="stack">
      <div className="policy_card">
      <div className="card_header active">
      <div className="card_num">1</div>
      Florida Blue FB9999
    </div>
    <div className="card_body">
      <table>
      <tr>
      <td>
      Policy Number<br/>
    <strong>123456789</strong>
    </td>
    <td>
    Effective Date<br/>
    <strong>12/31/2015</strong>
    </td>
    </tr>
    <tr>
    <td>
    Term Date<br/>
    <strong>N/A</strong>
    </td>
    <td>
    Insurance Type<br/>
    <strong>Individual</strong>
    </td>
    </tr>
    <tr>
    <td>
    WIP<br/>
    <strong>N/A</strong>
    </td>
    <td>&nbsp;

  </td>
    </tr>
    <tr>
    <td>&nbsp;

  </td>
    <td className="edit">
      <i className="fa fa-edit" aria-hidden="true"></i>
      </td>
      </tr>
      </table>

      </div>
      </div>
      <div className="policy_card">
      <div className="card_header active">
      <div className="card_num">2</div>
      Aetna
      </div>
      <div className="card_body">
      <table>
      <tr>
      <td>
      Policy Number<br/>
    <strong>123456789</strong>
    </td>
    <td>
    Effective Date<br/>
    <strong>12/31/2015</strong>
    </td>
    </tr>
    <tr>
    <td>
    Term Date<br/>
    <strong>N/A</strong>
    </td>
    <td>
    Insurance Type<br/>
    <strong>Individual</strong>
    </td>
    </tr>
    <tr>
    <td>
    WIP<br/>
    <strong>N/A</strong>
    </td>
    <td>&nbsp;

  </td>
    </tr>
    <tr>
    <td>&nbsp;

  </td>
    <td className="edit">
      <i className="fa fa-edit" aria-hidden="true" onClick={this.clickHandler}></i>
      </td>
      </tr>
      </table>

      </div>
      </div>
      <div className="policy_card">
      <div className="card_header inactive">
      <div className="card_num"><i className="fa fa-clock-o" aria-hidden="true"></i></div>
      Florida Blue FB9999
    </div>
    <div className="card_body">
      <table>
      <tr>
      <td>
      Policy Number<br/>
    <strong>123456789</strong>
    </td>
    <td>
    Effective Date<br/>
    <strong>12/31/2015</strong>
    </td>
    </tr>
    <tr>
    <td>
    Term Date<br/>
    <strong>N/A</strong>
    </td>
    <td>
    Insurance Type<br/>
    <strong>Individual</strong>
    </td>
    </tr>
    <tr>
    <td>
    WIP<br/>
    <strong>N/A</strong>
    </td>
    <td>&nbsp;

  </td>
    </tr>
    </table>
    </div>
    </div>
    <div className="policy_card_add">
      <div className="card_header">
      &nbsp;
  </div>
    <div className="card_body">
      <div className="btn_add"><i className="fa fa-plus" aria-hidden="true"></i>Add Policy</div>
    </div>
    </div>

    </div>
      </div>
    );
  }

  showDetails(){
    document.getElementById('policy_details_div').style.display='block';
    document.getElementById('policy_details_div').style.transform='scale(1)';
    document.getElementById('policy_details_div').style.right='10px';
    document.getElementById('policy_details_div').style.opacity='1';
  }

  clickHandler (event) {
    //this.props.routerActions.push('/member');
    console.log('policy screen');
    // minimize the policy div
    document.getElementById('policies_div').style.transform='scale(.3)';
    document.getElementById('policies_div').style.top='200px';
    document.getElementById('policies_div').style.right='250px';
    document.getElementById('policies_div').style.opacity='0';
    setTimeout(this.showDetails, 800)
    // show and maximize the details div

    // hide the policy div and reset its css
    //document.getElementById('policies_div').style.display = 'none';
    //document.getElementById('policies_div').style.transform='scale(1)';
    //document.getElementById('policies_div').style.top='100px';
    //document.getElementById('policies_div').style.right='0px';
    //document.getElementById('policies_div').style.opacity='1';
  }
}

export default connect(state=>state,routerActions)(Policies);



