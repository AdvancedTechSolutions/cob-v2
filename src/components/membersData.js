export default  [{
  id: 1,
  user: {
    hccNum: 'HCC1234XXXX',
    firstName: 'Roxanne',
    lastName: 'Polite',
    consumerID: '823120034',
    personNum: '01',
    DOB: '01/23/1975',
    gender: 'Female',
    medicareHIC: ''
  }
}, {
  id: 2,
    user: {
    hccNum: 'HCC1234XXXX',
      firstName: 'William',
      lastName: 'Buckley',
      consumerID: '1234567989',
      personNum: '03',
      DOB: '08/20/1965',
      gender: 'Male',
      medicareHIC: ''
  }
}, {
  id: 3,
  user: {
    hccNum: 'HCC1234XXXX',
    firstName: 'Katy',
    lastName: 'McGary',
    consumerID: '527348765',
    personNum: '03',
    DOB: '14/12/1965',
    gender: 'Female',
    medicareHIC: ''
  }
}, {
  id: 4,
  user: {
    hccNum: 'HCC1234XXXX',
    firstName: 'Baxter',
    lastName: 'Hennessy',
    consumerID: '527348765',
    personNum: '03',
    DOB: '14/12/1965',
    gender: 'Male',
    medicareHIC: ''
  }
}, {
  id: 5,
  user: {
    hccNum: 'HCC1234XXXX',
    firstName: 'Roxanne',
    lastName: 'Polite',
    consumerID: '527348765',
    personNum: '03',
    DOB: '14/12/1965',
    gender: 'Female',
    medicareHIC: ''
  }
}, {
  id: 6,
  user: {
    hccNum: 'HCC1234XXXX',
    firstName: 'Blake',
    lastName: 'Shelton',
    consumerID: '527348765',
    personNum: '03',
    DOB: '14/12/1965',
    gender: 'Male',
    medicareHIC: ''
  }
}, {
  id: 7,
  user: {
    hccNum: 'HCC1234XXXX',
    firstName: 'Anastasia',
    lastName: 'Demetra',
    consumerID: '527348765',
    personNum: '03',
    DOB: '14/12/1965',
    gender: 'Female',
    medicareHIC: ''
  }
}, {
  id: 8,
  user: {
    hccNum: 'HCC1234XXXX',
    firstName: 'Joan',
    lastName: 'Baker',
    consumerID: '527348765',
    personNum: '03',
    DOB: '14/12/1965',
    gender: 'Female',
    medicareHIC: ''
  }
}, {
  id: 9,
  user: {
    hccNum: 'HCC1234XXXX',
    firstName: 'Tom',
    lastName: 'Curran',
    consumerID: '527348765',
    personNum: '03',
    DOB: '14/12/1965',
    gender: 'Male',
    medicareHIC: ''
  }
}, {
  id: 10,
  user: {
    hccNum: 'HCC1234XXXX',
    firstName: 'Patrick',
    lastName: 'Stewart',
    consumerID: '527348765',
    personNum: '03',
    DOB: '14/12/1965',
    gender: 'Male',
    medicareHIC: ''
  }
}

]



