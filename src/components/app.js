import React, { Component, PropTypes } from 'react';
import FlipCard from 'react-flipcard';
import Header from './header';
import Footer from './footer';
import PolicyContainer from './policy_container';
import Hool from './hool';
import Dool from './dool';
import Correspondence from './correspondence';
import Accidents from './accidents'
import Claims from './claims_screen';
import ClaimsDetail from './claims_detail';
import '../styles/style.scss';
import '../styles/head_foot.css';

const styles = {
  logo:{
    position: 'fixed',
    bottom: 0,
    left: 0,
    width: '100%',
    maxWidth: '500px',
  },
  childContainer:{
    height: '100%',
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    margin: '0px 0px 0px 0px',
    backgroundColor: 'ff0000',
  },
};

const paths = {
  headerLogoPath: 'images/footer_logo.png',
  headerImage1Path: 'images/home.png',
  headerImage2Path: 'images/back.png',
  headerImage3Path: 'images/profile_pic.png',
  footerLogoPath: 'images/footer_logo.png',
  footerImage1Path: 'images/footer_back.png',
}

export default class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
  };

  constructor(){
    super();
  }

  render() {
    return (
      <div>
        <div>
          <Header
            logoImage={paths.headerLogoPath}
            headerImage1={paths.headerImage1Path}
            headerImage2={paths.headerImage2Path}
            headerImage3={paths.headerImage3Path}
          />
          <div style={styles.childContainer}>
            {this.props.children}
          </div>
          <PolicyContainer />
          <Hool/>
          <Dool/>
          <Claims/>
          <ClaimsDetail/>
          <Correspondence/>
          <Accidents/>
          <Footer
            footerlogoImage={paths.footerLogoPath}
            footerImage1={paths.footerImage1Path}
          />
        </div>
      </div>

    );
  }
}

export default App;
