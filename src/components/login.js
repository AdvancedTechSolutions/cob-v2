import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'
import '../styles/style.scss';
import '../styles/login.css';

const styles = {
  btn_login: {
    backgroundColor: '#58b651',
    color: '#fff',
    position: 'relative',
    padding: '3px 10px 3px 10px',
    borderRadius: 'none'
  },
  div_login: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '300px',
    marginTop: '150px'
  }
};

class Login extends Component {

  render(){
    return (
      <div className="login_div">
        <image src="images/logo_lg.png"/><br/>
        <input type="text" className="text"/><br/>
        <input type="text" className="text"/><br/>
        <Link to="/work"><input type="button" className="login_btn" value="Login"/></Link>
        <span className="note">Need Help</span>
      </div>
    )
  }
}

export default Login
