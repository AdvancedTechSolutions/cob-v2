import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'
import SearchInput, {createFilter} from 'react-search-input'
import membersData from './membersData'
import '../styles/style.scss';
import '../styles/memberSearch.css';

const styles = {
  div_member_search: {
    marginLeft: 'auto',
    marginRight: 'auto',
    textAlign: 'center',
    width: '635px',
    marginTop: '70px'
  },
  div_cancel: {
    textAlign: 'right',
    margin: '0px',
    color: '#666'
  }
};

const KEYS_TO_FILTERS = [
  'user.firstName',
  'user.lastName'
]




class MemberSearch extends Component {
  getInitialState () {
    return { searchTerm: '' }
  }

  constructor (props) {
    super(props);
    this.state = {searchTerm: ''};
    this.showQLink = this.showQLink.bind(this);
    this.searchUpdated = this.searchUpdated.bind(this);
    //this.state.searchTerm = this.state.searchTerm.bind(this);
  }

  render(){

    const filteredEmails = membersData.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

    return (
      <div id="memberSearchDiv" style={styles.div_member_search}>

        <h1>Member Search</h1>

        <div style={styles.div_cancel}><Link to="/"><i className="fa fa-times" aria-hidden="true"></i></Link></div>
        <div>
          <SearchInput className="search-input" onChange={this.searchUpdated} />
          <h2>Please search by Name, HCC ID, SSN, or Date of Birth</h2>
          <table>
            <thead>
              <tr>
                <th>&nbsp;</th>
                <th>CONSUMER ID</th>
                <th>FIRST NAME</th>
                <th>LAST NAME</th>
                <th>PERSON #</th>
                <th>DOB</th>
                <th>GENDER</th>
                <th>MEDICARE (HIC)</th>
              </tr>
            </thead>
            <tbody>
          {filteredEmails.map(member => {
            return (
              <tr key={member.id}><td>{member.user.hccNum}</td><td>{member.user.consumerID}</td><td> <Link to="/member" onClick={this.showQLink}>{member.user.firstName}</Link></td><td> <Link to="/member" onClick={this.showQLink}>{member.user.lastName}</Link></td><td>{member.user.personNum}</td><td>{member.user.DOB}</td><td>{member.user.gender}</td><td>{member.user.medicareHIC}</td></tr>
            )
          })}
          </tbody></table>
        </div>
      </div>
    )
  }


  showQLink (event) {
    //this.props.routerActions.push('/member');
    console.log('show q link');
    document.getElementById('footer').style.visibility = 'visible';
  }
  searchUpdated (term) {
    this.setState({searchTerm: term})
  }
}

export default MemberSearch
