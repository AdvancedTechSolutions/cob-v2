import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import Breadcrumbs from './BreadCrumbs';

const styles = {
  topBar: {
    backgroundColor: 'transparent',
    position: 'relative',
    padding: '20px 25px 0px'
  }
};

class WorkQHeader extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  toggles(obj){
    this.state = {};
    this.setState(obj);
  }

  render(){
    return (
      <div className='top-bar' style={styles.topBar}>
        <div className='top-bar-left'>
          {/*<Breadcrumbs />*/}<Link to="/work" style={{fontSize:'14px',color: '#fff'}}><i className="fa fa-times" aria-hidden="true"></i></Link>
        </div>
      <div className='top-bar-right'>
      <div className='pull-right'>
      <img src={this.props.logoImage} alt='' style={{height: '50px', float: 'left', margin: '3px 15px 0px 0px'}} />

    <Link to='/login' className='headerBtn'>
      <img src={this.props.headerImage1} />
  </Link>
    <Link to='/work' className='headerAvatar'>
      <img src={this.props.headerImage3} />
  </Link>
    </div>
    </div>
    </div>
  );
  }
}

export default connect(state=>state,routerActions)(WorkQHeader);
