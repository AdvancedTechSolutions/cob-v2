import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import FlipCard from 'react-flipcard';
import Policies from './policy_screen';
import PolicyDetail from './policy_detail';
import '../styles/policies.css';

const styles = {
  topBar: {
    backgroundColor: 'transparent',
    position: 'relative',
    padding: '20px 25px 0px'
  }
};

class PolicyContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      screenState: 'policy_screen'
    };
    this.clickHandler = this.clickHandler.bind(this);
  }

  toggles(obj){
    this.state = {};
    this.setState(obj);
  }

 render(){
    return (
        <div>
          <Policies/>
          <PolicyDetail/>
        </div>
    );
  }
  clickHandler (event) {
    //this.props.routerActions.push('/member');
    console.log('policy_container');
    document.getElementById('policies_div').style.display = 'none';
    document.getElementById('policy_details_div').style.display = 'block';
  }
}

export default connect(state=>state,routerActions)(PolicyContainer);



