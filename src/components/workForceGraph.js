import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import Force from 'utils/d3/html/force';
import SvgDefs from 'utils/d3/defs';
import rd3 from 'react-d3-library';
const RD3Component = rd3.Component;
import { routerActions } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import * as nodeActions from '../redux/modules/workNodes';
import SiebelWidget from './siebelwidget';

const paths = {
  siebelWidgetImgPath: 'images/ph_siebel_active.png',
  siebelWidgetLinkPath: '/workQ'
}

const styles = {
  links_div: {
    marginRight: '200px',
    marginLeft: '200px',
    marginTop: '200px'
  }
};
class workForceGraph extends Component {
  static propTypes = {
    workNodes: PropTypes.object,
    nodeActions: PropTypes.object,
    router: PropTypes.object,
  };

  constructor (props) {
    super(props);

    this.state = {d3: ''};
  }

  componentDidUpdate () {
    const currentNodeId = this.props.workNodes.workTree.id;
    this.props.nodeActions.selectWorkNode(decodeURI(currentNodeId));
  }

  componentDidMount () {
    console.log(this.props.workNodes);
    const graph = new Force(this.props.workNodes.workTree);
    const svgDefs = new SvgDefs(graph.svgStage);
    svgDefs.avatarClip().startArrow().endCircle();
    this.setState({d3: graph.node});
  }

  render () {
    return (
      <div>
        <div>
          <div>
            <RD3Component data={this.state.d3} />
          </div>
          <div className='pin-area'></div>
      <SiebelWidget
    siebelWidgetImg={paths.siebelWidgetImgPath}
    siebelWidgetLink={paths.siebelWidgetLinkPath}
  />
        </div>
      </div>
    );
  }
}

 export default connect(({ workNodes, router }) => ({ workNodes, router }),
 dispatch => ({
 routerActions: bindActionCreators(routerActions, dispatch),
 nodeActions: bindActionCreators(nodeActions, dispatch),
 })
 )(workForceGraph);


