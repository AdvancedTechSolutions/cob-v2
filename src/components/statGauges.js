import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import CircularProgressbar from 'react-circular-progressbar';
import Breadcrumbs from './BreadCrumbs';

const styles = {
  topBar: {
    backgroundColor: 'transparent',
    position: 'relative',
    padding: '20px 25px 0px'
  }
};

class StatGauges extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  toggles(obj){
    this.state = {};
    this.setState(obj);
  }


  render(){
    return (
      <div id="" className="stat_gauges">
        <div className="gauge">
          <div className="perc_0_24"><CircularProgressbar strokeWidth={10} percentage={22} initialAnimation={true} /></div>
          <div className="gaugeText">
            <h3>Stat Name</h3>
            Lorem Ipsum Dolor Sit Amet
          </div>
        </div>
        <div className="gauge">
          <div className="perc_75_99"><CircularProgressbar strokeWidth={10} percentage={75} initialAnimation={true} /></div>
          <div className="gaugeText">
            <h3>Stat Name</h3>
            Lorem Ipsum Dolor Sit Amet
          </div>
        </div>
        <div className="gauge">
          <div className="perc_0_24"><CircularProgressbar strokeWidth={10} percentage={22} initialAnimation={true} /></div>
          <div className="gaugeText">
            <h3>Stat Name</h3>
            Lorem Ipsum Dolor Sit Amet
          </div>
        </div>
        <div className="gauge">
          <div className="perc_25_49"><CircularProgressbar strokeWidth={10} percentage={45} initialAnimation={true} /></div>
          <div className="gaugeText">
            <h3>Stat Name</h3>
            Lorem Ipsum Dolor Sit Amet
          </div>
        </div>
        <div className="gauge">
          <div className="perc_50_74"><CircularProgressbar strokeWidth={10} percentage={53} initialAnimation={true} /></div>
          <div className="gaugeText">
            <h3>Stat Name</h3>
            Lorem Ipsum Dolor Sit Amet
          </div>
        </div>
        <div className="gauge">
          <div className="perc_75_99"><CircularProgressbar strokeWidth={10} percentage={84} initialAnimation={true} /></div>
          <div className="gaugeText">
            <h3>Stat Name</h3>
            Lorem Ipsum Dolor Sit Amet
          </div>
        </div>
        <div className="gauge">
          <div className="perc_0_24"><CircularProgressbar strokeWidth={10} percentage={22} initialAnimation={true} /></div>
          <div className="gaugeText">
            <h3>Stat Name</h3>
            Lorem Ipsum Dolor Sit Amet
          </div>
        </div>
    </div>
    );
  }
}



export default connect(state=>state,routerActions)(StatGauges);
