import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'
import '../styles/style.scss';
import '../styles/workQ.css';

const styles = {
  btn_login: {
    backgroundColor: '#58b651',
    color: '#fff',
    position: 'relative',
    padding: '3px 10px 3px 10px',
    borderRadius: 'none'
  },
  div_login: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '300px',
    marginTop: '150px'
  }
};

class Workqsearch extends Component {

  render(){
    return (
        <div id="workQSearchForm" className="searchForm"><img src="images/ph_workQSearchForm.png" onClick={this.workQSearchHide}/></div>
    )
  }
}

export default Workqsearch
