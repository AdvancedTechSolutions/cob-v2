import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import Breadcrumbs from './BreadCrumbs';

const styles = {
  notepad_pad: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: '0px',
    right: '0px',
    margin: '0px 20px 25px 0px'
  },
};

class PhLinks extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    return (
      <div className="linkDiv" style={{position:'absolute',top:'230px',left: '100px'}}>
        <Link to="/vador">Vador</Link><br/>
        <Link to="/membersearch">Member Search</Link><br/>
        <Link to="/workQ">Work Queue</Link><br/>
        <Link to="/lastinteractions">My Last Interactions</Link>
      </div>
    );
  }
}

export default PhLinks;
