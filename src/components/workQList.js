import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'
import Workqlistitems from '../components/workQListItems'
import '../styles/style.scss';
import '../styles/workQ.css';

class Workqlist extends Component {

  render(){
    return (
      <div className="workQList">
        <div className="search" onClick={this.searchShow}>
          <img src="images/ph_workQSearch.png"/>
        </div>
        <div id="workQSearchForm" className="searchForm">
          <img src="images/ph_workQSearchForm.png" onClick={this.searchHide}/>
        </div>
        <div id="" className="filter"><img src="images/ph_workQFilter.png"/></div>
        <Workqlistitems/>
        <br className="clear"/>
      </div>
    )
  }

  searchShow() {
    document.getElementById('workQSearchForm').style.display ='none';
  }
  searchHide() {
    document.getElementById('workQSearchForm').style.display ='block';
  }
}

export default Workqlist
