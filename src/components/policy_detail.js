import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import Breadcrumbs from './BreadCrumbs';
import '../styles/policyDetail.css';

const styles = {
  topBar: {
    backgroundColor: 'transparent',
    position: 'relative',
    padding: '20px 25px 0px'
  }
};

class PolicyDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {};
    this.clickHandler = this.clickHandler.bind(this);
  }

  toggles(obj){
    this.state = {};
    this.setState(obj);
  }

  render(){
    return (
      <div id="policy_details_div" className="policy_container collapsed">
      <div className="stack">
      <div className="position">
      <div className="policy_card">
      <div className="card_header active">
      <div className="card_num">1</div>
      Florida Blue FB9999
    </div>
    <div className="card_body">
      Policy Number<br/>
    <strong>123456789</strong>
    </div>
    </div>
    <div className="policy_card">
      <div className="card_header inactive">
      <div className="card_num"><i className="fa fa-clock-o" aria-hidden="true"></i></div>
      Florida Blue FB9999
    </div>
    <div className="card_body">
      Policy Number<br/>
    <strong>123456789</strong>
    </div>
    </div>
    <div className="btn_add"><i className="fa fa-plus" aria-hidden="true"></i>Add Policy</div>
    </div>
    </div>
    <div className="details">
      <div className="policy_header">
      <table>
      <tr>
      <td className="title">Aetna - Policy Details</td>
    <td className="actions"><span  onClick={this.clickHandler}>Cancel</span> <div className="btn_save" onClick={this.clickHandler}><i className="fa fa-floppy-o" aria-hidden="true"></i>Save Changes</div></td>                </tr>
    </table>
    </div>
    <div className="policy_row">
      <table>
      <tr>
      <th colSpan="3"><i className="fa fa-chevron-down" aria-hidden="true"></i>Carrier</th>
      </tr>
      <tr>
      <td>Carrier Name<br/>
    <input type="text" defaultValue="Aetna"/></td>
      <td>Carrier Code<br/>
    <input type="text" value="00K79"/></td>
      <td>Carrier Location<br/>
    <input type="text" value="Richmond, VA"/></td>
      </tr><tr>
      <th colSpan="3"><i className="fa fa-chevron-down" aria-hidden="true"></i>Policy Info</th>
    </tr>
    <tr>
    <td>Policy Type<br/>
    <input type="text" defaultValue="Health"/></td>
      <td>Insurance Type<br/>
    <input type="text" defaultValue="Individual"/></td>
      <td>Coverage Code<br/>
    <input type="text" defaultValue="1212121212555"/></td>
      </tr>
      <tr>
      <td>Policy Number<br/>
    <input type="text" value="081119872"/></td>
      <td>Policy Effective Date<br/>
    <input type="text" value="01/05/2015"/></td>
      <td>Policy Term Date<br/>
    <input type="text" value="12/31/2016"/></td>
      </tr>
      <tr>
      <td>Group Number<br/>
    <input type="text" value="12345"/></td>
      <td>Group Name Type<br/>
    <input type="text" value="2"/></td>
      <td>Tax ID Number<br/>
    <input type="text"  value="123-35-7654"/></td>
      </tr>
      <tr>
      <td>MBR Effective Date<br/>
    <input type="text" value="01/05/2015"/></td>
      <td>MBR Term Date Type<br/>
    <input type="text" value="01/05/2015"/></td>
      <td>State Rule Override<br/>
    <input type="text" value="N/A"/></td>
      </tr>
      <tr>
      <td>COB Ind<br/>
    <input type="text" value="01"/></td>
      <td>Cobra Ind<br/>
    <input type="text" value="18"/></td>
      <td>Dependent Liability Rule<br/>
    <input type="text" value="N/A"/></td>
      </tr>
      <tr>
      <td>Race<br/>
    <input type="text" value="N/A"/></td>
      <td>Written Language<br/>
    <input type="text" value="English"/></td>
      <td>Spoken Language<br/>
    <input type="text" value="English"/></td>
      </tr>

      </table>
      </div>

      </div>


      </div>

  );
  }
  clickHandler (event) {
    //this.props.routerActions.push('/member');
    console.log('policy detail');
    document.getElementById('policy_details_div').style.transform='scale(.2)';
    document.getElementById('policy_details_div').style.right='100px';
    document.getElementById('policy_details_div').style.opacity='0';
    document.getElementById('policy_details_div').style.display = 'none';
    document.getElementById('policies_div').style.transform='scale(1)';
    document.getElementById('policies_div').style.top='100px';
    document.getElementById('policies_div').style.right='0px';
    document.getElementById('policies_div').style.opacity='1';
    document.getElementById('policies_div').style.display = 'block';
  }
}

export default connect(state=>state,routerActions)(PolicyDetail);







