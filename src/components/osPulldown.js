import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'
import '../styles/style.scss';
import '../styles/os.css';


class OsPulldown extends Component {

  render(){
    return (
      <div id="os_pulldown_div" className="os_div">
        <div className="screen_container">
          <div className="screen">
            <image src="images/ph_osPulldown.png" onClick={this.osHide}/>
          </div>
        </div>
        <div className="tab_container">
          <div id="os_pulldown_tab" className="tab" onClick={this.osShow}>
            <i className="fa fa-chevron-down" aria-hidden="true"></i>
          </div>
        </div>
      </div>
    )
  }

  osShow() {
    document.getElementById('os_pulldown_div').style.top ='0px';
    document.getElementById('os_pulldown_tab').style.display ='none';
  }
  osHide() {
    document.getElementById('os_pulldown_div').style.top ='-504px';
    document.getElementById('os_pulldown_tab').style.display ='block';
  }
}

export default OsPulldown
