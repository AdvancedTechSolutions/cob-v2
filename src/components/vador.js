import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router'
import '../styles/style.scss';
import '../styles/vador.css';

const styles = {
  vador_container: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '635px',
    marginTop: '150px',
    backgroundColor: '#576f8c',
    color: '#fff',
    padding: '3px 10px 3px 10px',
    borderRadius: '10px'
  },
  vador_close: {
    textAlign: 'right',
  }
};

class Vador extends Component {

  render(){
    return (
      <div className="vador_container">
        <div className="vador_close"><Link to="/work">X</Link></div>
        <div className="vador_title"><h1>Vador</h1></div>
        <table>
          <tbody>
            <tr>
              <td className="left">File Date (MM-DD-YYYY)<br/>
              <input type="text" className="form_text"></input></td>
              <td className="right">CMS Contract Number<br/>
    <input type="text" className="form_text"></input></td>
            </tr>
            <tr>
              <td className="left">HICN Number<br/>
                <input type="text" className="form_text"></input></td>
              <td className="right">Supplemental Type Number<br/>
                <input type="text" className="form_text"></input></td>
            </tr>
            <tr>
              <td className="left">SSN<br/>
                <input type="text" className="form_text"></input></td>
              <td className="right">ICAR Code<br/>
                <input type="text" className="form_text"></input></td>
            </tr>
            <tr>
              <td className="left_last">Florida Blue Contract Number<br/>
                <input type="text" className="form_text"></input></td>
              <td className="right_last">Error Codes<br/>
                <input type="text" className="form_text"></input></td>
            </tr>
            <tr>
              <td className="cancel">
                <input type="button" className="cancel_btn" value="Cancel"/> Clear
              </td>
              <td className="search">
                <Link to="/vadorresults"><input type="button" className="search_btn" value="Search"/></Link>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

export default Vador
