import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Slider from "react-slick";
import '../styles/style.scss';
import classes from '../styles/queue.scss';

const styles = {
  ele:{
    width: '25vw',
    color: "green"
  },
  colo:{
    "margin-right": "30px",
    "margin-left": "30px"
  }
};


class Parent extends Component{
  constructor(props) {
    super(props);

  }
}

class Excerpts extends Component{
  render(){
    return (
      <div className='fullExcerpt'>
        <div className='excerptHeader'>
          <image src="/images/card_diamond.png" style={{float:'left', margin:'0px 10px 0px 0px'}}/>
          <h1>Claim Exception</h1>
    <i className="fa fa-envelope" aria-hidden="true"></i>&nbsp;Received from Diamond

        </div>
        <div className="otherContent">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet fringilla risus. Pellentesque luctus eros ac quam sodales, sed sollicitudin enim mollis. Duis fringilla bibendum lacinia. </p>

    <p>Nullam euismod, ligula eget pretium finibus, justo nunc convallis nisi, sit amet porttitor ligula ipsum quis est. Integer semper fringilla molestie. Nulla rhoncus placerat quam semper vulputate. Mauris non tincidunt neque. Vivamus posuere ipsum dignissim, sollicitudin quam in, posuere erat. In id lobortis erat. Donec hendrerit eros ligula, sit amet congue nunc mollis vel. </p>
          <div className="form_div">
            <div className="form_box">
              Comments<br/>
              <input type="textarea"/>
            </div>
            <div className="form_btn">
              <input type="button" className="enter_btn" value="Enter"/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
class Excerpts2 extends Component{
  render(){
    return (
      <div className='fullExcerpt'>
      <div className='excerptHeader green'>
      <image src="/images/card_diamond.png" style={{float:'left', margin:'0px 10px 0px 0px'}}/>
      <h1>Claim Exception</h1>
        <i className="fa fa-envelope" aria-hidden="true"></i>&nbsp;Received from Diamond

        </div>
        <div className="otherContent">
        <p>Nullam euismod, ligula eget pretium finibus, justo nunc convallis nisi, sit amet porttitor ligula ipsum quis est. Integer semper fringilla molestie. Nulla rhoncus placerat quam semper vulputate. Mauris non tincidunt neque. Vivamus posuere ipsum dignissim, sollicitudin quam in, posuere erat. </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet fringilla risus. Pellentesque luctus eros ac quam sodales, sed sollicitudin enim mollis. Duis fringilla bibendum lacinia. </p>
        <div className="form_div">
      <div className="form_box">
      Comments<br/>
      <input type="textarea"/>
      </div>
      <div className="form_btn">
      <input type="button" className="enter_btn" value="Enter"/>
      </div>
      </div>
      </div>
      </div>
  );
  }
}

class Slide extends Parent{

  constructor(props) {
    console.log(props);
    super(props);
  }

  componentDidMount(){
    var d =this;
    setTimeout(function(){d.forceUpdate();}, 100);
  }

  render() {
    const settings = {
      arrows: 'false',
      className: 'center',
      centerMode: true,
      infinite: true,
      centerPadding: '0px',
      slidesToShow: 3,
      speed: 500,
      variableWidth: true
    };
    return (
      <div>
      <Slider {...settings}>
  <div><Excerpts style={styles.colo}/></div>
    <div><Excerpts style={styles.colo}/></div>
    <div><Excerpts2 style={styles.colo}/></div>
    <div><Excerpts style={styles.colo}/></div>
    <div><Excerpts2 style={styles.colo}/></div>
    <div><Excerpts style={styles.colo}/></div>
    <div><Excerpts2 style={styles.colo}/></div>
    </Slider>
    </div>
  );
  }
};


class workQArea extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render(){
    return (
      <div >
      <div className="views">
      <Slide/>
      </div>

      <br className="clear"/>
      </div>
  );
  }
}

export default connect(state=>state,routerActions)(workQArea);


