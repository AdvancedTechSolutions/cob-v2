import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { routerActions } from 'react-router-redux';
import Fa from 'react-fa';
import Breadcrumbs from './BreadCrumbs';

const styles = {
  siebel_widget: {
    backgroundColor: 'transparent',
    position: 'absolute',
    top: '100px',
    left: '0px',
    margin: '0px 0px 25px 20px'
  },
};

class SiebelWidget extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    return (
      <div className='siebel_widget' style={styles.siebel_widget}>
        <Link to={this.props.siebelWidgetLink}><img src={this.props.siebelWidgetImg} /></Link>
      </div>
    );
  }
}

export default SiebelWidget;
