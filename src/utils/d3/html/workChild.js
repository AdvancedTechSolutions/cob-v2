import React, { Component, PropTypes } from 'react';
import { routerActions } from 'react-router-redux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as nodeActions from '../../../redux/modules/workNodes';
import toDataUrl from 'to-data-url';

class WorkChild extends Component {
  static propTypes = {
    id: PropTypes.string,
    type: PropTypes.string,
    workNodes: PropTypes.object,
    routerActions: PropTypes.object,
    nodeActions: PropTypes.object,
    update: PropTypes.func,
  };

  constructor (props) {
    super(props);

    this.clickHandler = this.clickHandler.bind(this);
    const node = {...this.props, ...this.props.workNodes[this.props.id]};
  }

  render () {
    const node = {...this.props, ...this.props.workNodes[this.props.id]};
    return (

      <div className={`${node.type || this.props.type || 'workChild'} ${node.style}`} onClick={this.clickHandler}>
        {node.icon && <img className='icon' src={node.icon} alt={node.label} />}
        {/*<div className='node-label'>{node.label}</div>*/}
      </div>
    );
  }

  componentWillMount () {
    if (!this.props.workNodes[this.props.id].r) {
      this.props.nodeActions.updateWorkTreeNode(this.props.id, { r: true });
    }
  }

  clickHandler () {
    if (event.defaultPrevented) return;
    let node = {...this.props, ...this.props.workNodes[this.props.id]};

    if(node.external){
      window.open(node.external);
      return;
    }
    if(node.go){
      console.log(node.go);
      this.props.routerActions.push(node.go);
      return;
    }
    let path = '';
    do {
      path = `/${node.id}${path}`;
      node = node._parent;
    } while (node);
    this.props.routerActions.push(`${path}`);
    this.props.update();
  }
}

export default {
  enter: connect(
    ({ workNodes: { workNodes } }) => ({ workNodes }),
    dispatch => ({
      routerActions: bindActionCreators(routerActions, dispatch),
      nodeActions: bindActionCreators(nodeActions, dispatch),
    })
  )(WorkChild),
};
