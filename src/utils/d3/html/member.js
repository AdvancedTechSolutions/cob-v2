import React, { Component, PropTypes} from 'react';
import Actions from './actions';
import dateFormat from 'date-format';
import { routerActions } from 'react-router-redux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as nodeActions from '../../../redux/modules/nodes';

class Member extends Component {
  static propTypes = {
    type: PropTypes.string,
    id: PropTypes.string,
    nodes: PropTypes.object,
    routerActions: PropTypes.object,
    nodeActions: PropTypes.object,
    update: PropTypes.func,
  };

  constructor (props) {
    super(props);

    this.clickHandler = this.clickHandler.bind(this);
    this.state = {orginalName: this.props.nodes[this.props.id].name};
  }

  render () {
    const member = this.props.nodes[this.props.id];
    return (
      <div className={member.type || 'member'}>
        <div
          className='clickable'
          onClick={this.clickHandler}
        >
          <div className='row'>
            <div className='col-xs-12'>
              <img src={member.avatar} alt='avatar' className='avatar' />
            </div>
          </div>
          <div className='row'>
            <div className='col-xs-12'>
              <div className='lead node-label'>{member.name}</div>
              <div className='value'>Policy ID: <strong>{member.memberNumber}</strong></div>
            </div>
          </div>
          <div className='row'>
            <div className='col-xs-12'>
              <div className='node-label'>Date of Birth</div>
              <div className='value'><strong>{dateFormat('MM/dd/yyyy', new Date(member.dob))}</strong></div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  clickHandler (event) {
    if (event.defaultPrevented) return;
    let node = this.props;
    if(node.go){
      console.log(node.go);
      this.props.routerActions.push(node.go);
      return;
    }
    let path = '';
    do {
      path = `/${node.id}${path}`;
      node = node._parent;
    } while (node);
    this.props.routerActions.push(`${path}`);

    console.log('member clicked');

    // hack to hide all the other divs
    document.getElementById('policies_div').style.display = 'none';
    document.getElementById('hool_container').style.display = 'none';
    document.getElementById('policy_details_div').style.display = 'none';
    document.getElementById('dool_container').style.display = 'none';
    document.getElementById('claims_div').style.display = 'none';
    document.getElementById('claims_details_div').style.display = 'none';
    document.getElementById('correspond_container').style.display = 'none';
    document.getElementById('accidents_container').style.display = 'none';
    this.props.update('wide');
  }
}

export default {
  enter: connect(
    ({ nodes: { nodes } }) => ({ nodes }),
    dispatch => ({
      routerActions: bindActionCreators(routerActions, dispatch),
      nodeActions: bindActionCreators(nodeActions, dispatch),
    })
  )(Member),
};
