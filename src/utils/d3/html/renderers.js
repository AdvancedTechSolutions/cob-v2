import member from './member';
import dflt from './default';
import work from './work';
import workChild from './workChild';

export default {
  work,
  workChild,
  member,
  'default': dflt,
};
