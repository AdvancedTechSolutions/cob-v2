import React, { Component, PropTypes } from 'react';
import { routerActions } from 'react-router-redux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as nodeActions from '../../../redux/modules/nodes';
import toDataUrl from 'to-data-url';

class DefaultEnter extends Component {
  static propTypes = {
    id: PropTypes.string,
    type: PropTypes.string,
    nodes: PropTypes.object,
    routerActions: PropTypes.object,
    nodeActions: PropTypes.object,
    update: PropTypes.func,
  };

  constructor (props) {
    super(props);

    this.clickHandler = this.clickHandler.bind(this);
    const node = {...this.props, ...this.props.nodes[this.props.id]};
  }

  render () {
    const node = {...this.props, ...this.props.nodes[this.props.id]};
    return (
      <div
        className={`${node.type || this.props.type || 'default'} ${node.style}`}
        style={{backgroundColor: `${node.color}`}}
        onClick={this.clickHandler}
      >
        {node.icon && <img className='icon' src={node.icon} alt={node.label} />}
        <div className='node-label'>{node.label}</div>
      </div>
    );
  }

  componentWillMount () {
    if (!this.props.nodes[this.props.id].r) {
      this.props.nodeActions.updateTreeNode(this.props.id, { r: true });
    }
  }

  clickHandler () {
    if (event.defaultPrevented) return;
    let node = {...this.props, ...this.props.nodes[this.props.id]};
    // added to show div
    if(node.show){
      console.log('show div: '  + node.show);
      // hack to hide all the other divs
      document.getElementById('policies_div').style.display = 'none';
      document.getElementById('hool_container').style.display = 'none';
      document.getElementById('policy_details_div').style.display = 'none';
      document.getElementById('dool_container').style.display = 'none';
      document.getElementById('claims_div').style.display = 'none';
      document.getElementById('claims_details_div').style.display = 'none';
      document.getElementById('correspond_container').style.display = 'none';
      document.getElementById('accidents_container').style.display = 'none';
      // reset the animations on the policy div and screen
      document.getElementById('policies_div').style.transform='scale(1)';
      document.getElementById('policies_div').style.top='100px';
      document.getElementById('policies_div').style.right='0px';
      document.getElementById('policies_div').style.opacity='1';


      if(node.show != 'none') {
        document.getElementById(node.show).style.display = 'block';
      }
      //this.props.slideActions.slideIn(toDataUrl(this.img));

      this.props.update('narrow');
      return;
    }



    if(node.external){
      window.open(node.external);
      return;
    }
    if(node.go){
      console.log(node.go);
      this.props.routerActions.push(node.go);
      return;
    }
    let path = '';
    do {
      path = `/${node.id}${path}`;
      node = node._parent;
    } while (node);

    this.props.routerActions.push(`${path}`);
    this.props.update();
  }
}

export default {
  enter: connect(
    ({ nodes: { nodes } }) => ({ nodes }),
    dispatch => ({
      routerActions: bindActionCreators(routerActions, dispatch),
      nodeActions: bindActionCreators(nodeActions, dispatch),
    })
  )(DefaultEnter),
};
