import React, { Component, PropTypes} from 'react';
import Actions from './actions';
import dateFormat from 'date-format';
import { routerActions } from 'react-router-redux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as nodeActions from '../../../redux/modules/workNodes';

class Work extends Component {
  static propTypes = {
    type: PropTypes.string,
    id: PropTypes.string,
    nodes: PropTypes.object,
    routerActions: PropTypes.object,
    nodeActions: PropTypes.object,
    update: PropTypes.func,
  };

  constructor (props) {
    super(props);

    this.clickHandler = this.clickHandler.bind(this);
    this.state = {orginalName: this.props.workNodes[this.props.id].name};
  }

  render () {
    const work = this.props.workNodes[this.props.id];
    return (
        <div
          className='clickable'
          onClick={this.clickHandler}        >
              <img src={work.avatar} alt='avatar' className='work_avatar' />
        </div>
    );
  }

  clickHandler (event) {
    if (event.defaultPrevented) return;
    let node = this.props;
    let path = '';
    do {
      path = `/${node.id}${path}`;
      node = node._parent;
    } while (node);
    this.props.routerActions.push(`${path}`);
    this.props.update();
  }
}

export default {
  enter: connect(
    ({ workNodes: { workNodes } }) => ({ workNodes }),
    dispatch => ({
      routerActions: bindActionCreators(routerActions, dispatch),
      nodeActions: bindActionCreators(nodeActions, dispatch),
    })
  )(Work),
};
