import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import nodes from './modules/nodes';
import workNodes from './modules/workNodes';


export default combineReducers({
  router,
  workNodes,
  nodes,
});
