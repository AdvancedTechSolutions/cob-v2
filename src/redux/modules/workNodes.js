import workTree from '../../workNodeStructure.json';
import workNodes from '../../workNodeData.json';
// ------------------------------------
// Constants
// ------------------------------------
export const UPDATE_WORK_NODE = 'UPDATE_WORK_NODE';
export const UPDATE_WORK_TREE_NODE = 'UPDATE_WORK_TREE_NODE';
export const SELECT_WORK_NODE = 'SELECT_WORK_NODE';
export const TOGGLE_WORK_MENU = 'TOGGLE_WORK_MENU';

// ------------------------------------
// Actions
// ------------------------------------
export function updateWorkNode (id, value) {
  return {
    type: UPDATE_WORK_NODE,
    payload: {id, value},
  };
}
export function updateWorkTreeNode (id, value) {
  return {
    type: UPDATE_WORK_TREE_NODE,
    payload: {id, value},
  };
}

export function selectWorkNode (id) {
  return {
    type: SELECT_WORK_NODE,
    payload: {id},
  };
}

export function toggleWorkMenu (id) {
  return {
    type: TOGGLE_WORK_MENU,
    payload: {id},
  };
}

export const actions = {
  updateWorkNode,
  updateWorkTreeNode,
  selectWorkNode,
  toggleWorkMenu,
};

const findTreeNode = (node, id) => {
  if (node.id === id) {
    return node;
  }
  const children = node._children || node.children;
  if (children) {
    for (let index = 0; index < children.length; ++index) {
      const result = findTreeNode(children[index], id);
      if (result) {
        return result;
      }
    }
  }
  return false;
};

const reset = (node, parent) => {
  if (node.pinned) return;
  node._children = node._children || node.children || [];
  if (parent) {
    node._parent = node.parent = parent;
  }
  node.children = null;
  node.hidden = true;
  if (node._children) node._children.forEach(n => reset(n, node));
};

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [UPDATE_WORK_NODE]: (state, { payload: { id, value } }) => {
    const mergedNode = Object.assign({}, state.workNodes[id], value);
    const mergedNodes = Object.assign({}, state.workNodes, {[id]: mergedNode});
    return Object.assign({}, state, {nodes: mergedNodes});
  },
  [UPDATE_WORK_TREE_NODE]: (state, { payload: { id, value } }) => {
    const node = findTreeNode(state.workTree, id);
    if (node) {
      Object.assign(node, value);
    }
    return state;
  },
  [TOGGLE_WORK_MENU]: (state, { payload: { id } }) => {
    if (state.menu === id) {
      return {...state, menu: undefined};
    }
    return {...state, menu: id};
  },
  [SELECT_WORK_NODE]: (state, { payload: { id } }) => {
    reset(state.workTree);
    const selected = findTreeNode(state.workTree, id);
    if (selected) {
      selected.children = [...selected._children] || []; // Set the children of selected to the 'cached' children
      selected.children.forEach(child => (child.hidden = !!child.pinned)); // show the children of selected
      selected.hidden = false; // show the selected node
      if (selected._parent) {
        selected.parent = selected._parent; // set the parent of selected to the 'cached' parent
        selected.parent.children = [selected]; // set the parent to only have the selected node as the child
        selected.parent.hidden = false; // show the parent
        if (selected.parent._parent) {
          selected.parent.parent = state.workTree; // send the parent's parent to the root node, per requirements.
          selected.parent.parent.children = [selected.parent]; // set the parent's parent child to the selected parent
          selected.parent.parent.hidden = false; // show the root node.
        }
      }
    }
    return state;
  },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {workTree, workNodes};
export default function workNodesReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}
