import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import App from '../components/app';
import menuList from '../components/menuList';
import MemberSearch from '../components/memberSearch';
import horol from '../components/horol';
import hool from '../components/hool';
import dool from '../components/dool';
import claims from '../components/claims_screen';
import d3ForceGraph from '../components/d3ForceGraph';

export default (store) => (

  <Router>
    <Route path="/" component={App}>
      <IndexRoute component={menuList} />
      <Route path="/im" component={menuList}/>
      <Route path="/member" component={d3ForceGraph}>
        <Route path="*" component={d3ForceGraph} />
      </Route>
      <Route path="/MemberSearch" component={MemberSearch}/>
      <Route path="/hool" component={hool}/>
      <Route path="/dool" component={dool}/>
      <Route path="/claims" component={claims}/>
      <Route path="/horol" component={horol}/>
    </Route>
  </Router>
);



