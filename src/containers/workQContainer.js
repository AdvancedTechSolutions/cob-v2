import React, { Component, PropTypes } from 'react';
import Login from '../components/login';
import WorkQHeader from '../components/workqheader';
import Workqlist from '../components/workQList';
import Workqarea from '../components/workQArea';
import Workqareanoresults from '../components/workQAreaNoResults';
import '../styles/style.scss';
import '../styles/workQ.css';

const paths = {
  headerLogoPath: 'images/logo_sm_blue.png',
  headerImage1Path: 'images/logout_icon.png',
  headerImage2Path: 'images/window_icon.png',
  headerImage3Path: 'images/profile_icon_blue.png'
}

export default class workQContainer extends Component {


  get content () {
    return (
      <Router history={this.props.history}>
        {this.props.routes}
      </Router>
  );
  }

  render () {
    return (
      <div className="workQContainer">
        <div>
          <Workqlist/>
        </div>
        <div className="workQArea">
          <WorkQHeader
            logoImage={paths.headerLogoPath}
            headerImage1={paths.headerImage1Path}
            headerImage2={paths.headerImage2Path}
            headerImage3={paths.headerImage3Path}
          />
    {/* <Workqareanoresults /> */}
          <Workqarea />
        </div>
      </div>

    );
  }
}

export default workQContainer;
