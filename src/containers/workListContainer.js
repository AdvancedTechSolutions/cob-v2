/**
 * Created by mike on 9/14/2016.
 */
import React, { Component, PropTypes } from 'react';
import Header from '../components/header';
import Footer from '../components/footer';
import Policies from '../components/policy_screen';
import PolicyDetail from '../components/policy_detail';
import Hool from '../components/hool';
import '../styles/style.scss';
import '../styles/head_foot.css';

const styles = {
  logo:{
    position: 'fixed',
    bottom: 0,
    left: 0,
    width: '100%',
    maxWidth: '500px',
  },
  childContainer:{
    height: '100%',
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    margin: '40px 0px 0px 0px'
  },
};

const paths = {
  headerLogoPath: 'images/logo.png',
  headerImage1Path: 'images/home.png',
  headerImage2Path: 'images/back.png',
  headerImage3Path: 'images/profile_pic.png',
  footerLogoPath: 'images/footer_logo.png',
  footerImage1Path: 'images/footer_back.png',
}

export default class WorkListContainer extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
  };

  constructor(){
    super();
  }

  render() {
    return (
      <div className="Mike">
      <div>
      <Header
    logoImage={paths.headerLogoPath}
    headerImage1={paths.headerImage1Path}
    headerImage2={paths.headerImage2Path}
    headerImage3={paths.headerImage3Path}
  />
  <div style={styles.childContainer}>
    {this.props.children}
  </div>
    <Policies/>
    <PolicyDetail/>
    <Hool/>
    <Footer
    footerlogoImage={paths.footerLogoPath}
    footerImage1={paths.footerImage1Path}
  />
  </div>
    </div>

  );
  }
}

export default WorkListContainer;

